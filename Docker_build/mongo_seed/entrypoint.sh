#!/bin/bash

sleep 10

EXISTS=`echo 'show dbs' | mongo mongodb://mongo:27017/ | grep testdb`
if [ "a${EXISTS}" == "a" ]
then
    mongoimport --uri mongodb://mongo:27017/testdb --collection privacy_metrics --drop --file /privacy_metrics.json --jsonArray
fi