# Privacy Metrics

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

## 1. Introduction
Privacy Metrics represent the means to increase the user’s awareness. This component collects, computes and shares easy-to-understand data to allow users know how a service (e.g., a data buyer) stores and manages the data, if it shares it with third parties, how secure and transparent it looks, etc. These are all fundamental pieces of information for a user to know to take informed decisions. The PM computes this information via a standard REST  interface, offering an open knowledge information system which can be queried using an open and standard platform.  PMs combine information from supervised machine learning analytics, services themselves and domain experts, volunteers, and contributors.
The Open API implementation of Privacy Metrics component is available at official PIMCity’s Gitlab code repository, under [WP5 project](https://gitlab.com/pimcity/wp5/open-api/-/blob/master/WP2/privacy-metrics.yml).

For a complete description of the data contained in Privacy Metrics, refer to Sec. 4.3.1 of PIMCity's [Deliverable D2.2](https://www.pimcity-h2020.eu/app/uploads/2021/10/D2.2_Rev2_Final-design-and-preliminary-version-of-the-tools-to-improve-user%E2%80%99s-privacy.pdf). The corresponding data structure and schema are described in Privacy Metrics' Open API [implementation](https://gitlab.com/pimcity/wp5/open-api/-/blob/master/WP2/privacy-metrics.yml).

Privacy Metrics implements authorization based on OAuth2.0 model using Client Credentials flow.

In this repo we provide the implementation of the backend offering authenticated access to PMs. It builds on MongoDB (for the database), Python/Flask and [Swagger](https://swagger.io/) (for the server).
The repo builds on [Poetry](https://python-poetry.org/) for the management of Python packaging and dependencies.

![Module architecture](./misc/architecture.png)

## 2. Installation

## 2.1 Native deployment

#### 2.1.1 Pyenv, Python and Poetry

We recommend to install `pyenv` to install and manage different Python versions:

Install `pyenv` using [this guide](https://github.com/pyenv/pyenv)

Then, install Python3.9. In the following, we are using the latest version available (3.9.5) at the moment of this writing.

```sh
pyenv install 3.9.5
```

Set the installed version as global:

```sh
pyenv global 3.9.5
```

Now, install Poetry using [this guide](https://python-poetry.org/docs/).

Configure Poetry to use local virtual environments:

```sh
poetry config virtualenvs.in-project true
```

Then, install the project dependencies with `poetry`:

```sh
# Install dependencies
poetry install
```

<u>Note:</u> 
You can switch back to previous Python version with

```sh
# Switch back to system Python
pyenv global system
```

#### 2.1.2 MongoDB

On macOS, we recommend to install MongoDB with [Brew](https://brew.sh/):

```sh
brew install mongodb-community
```

On Ubuntu, you can install MongoDB with `apt`:

```sh
sudo apt install mongodb
```

Alternatively, you can install MongoDB using [this guide](https://docs.mongodb.com/manual/installation/).


### 2.1.3 Configuration and Execution

First, start MongoDB and import data. In a terminal, run the commands listed below.

macOS:

```sh
# Start MongoDB
brew services start mongodb/brew/mongodb-community
```

Ubuntu:

```sh
sudo service mongodb start
```

Once launched, populate the DB with Privacy Metrics data:

```
# Import data
mongoimport --db testdb --collection privacy_metrics --drop --file seed/privacy_metrics.json --jsonArray
```

Specify the IP address and port on which MongoDB is listening

```sh
export MONGO_HOST=localhost
export MONGO_PORT=27017
```

Specify the URLs for the server and for the retrieval of access tokens (these are just examples)

```sh
export SERVER_URL='https://easypims.pimcity-h2020.eu/'
export TOKEN_URL='https://easypims.pimcity-h2020.eu/identity/auth/realms/pimcity/protocol/openid-connect/token'
export AUTH_URL='https://easypims.pimcity-h2020.eu/identity/auth/'
```

Set environment and debug mode on if needed (default is off):

```sh
export FLASK_ENV=development
export SERVER_DEBUG=True
```

Then, run the API server

```sh
# Run the API server
poetry run python3 -m privacy_metrics.main
```

### 2.2 Deployment with Docker

This repo comes with the gears necessary to deploy the Privacy Metrics module in a dockered environment.

#### 2.2.1 Pyenv, Python and Poetry

We recommend to install `pyenv` to install and manage different Python versions:

Install `pyenv` using [this guide](https://github.com/pyenv/pyenv)

Then, install Python3.9. In the following, we are using the latest version available (3.9.5) at the moment of this writing.

```sh
pyenv install 3.9.5
```

Set the installed version as global:

```sh
pyenv global 3.9.5
```

Now, install Poetry using [this guide](https://python-poetry.org/docs/).

Configure Poetry to use local virtual environments:

```sh
poetry config virtualenvs.in-project true
```

#### 2.2.2 Docker

On macOS, we recommend to install Docker using [this guide](https://docs.docker.com/docker-for-mac/install/)

On Ubuntu, you can install Docker with `snap`

```sh
sudo snap install docker
```

### 2.2.3 Configuration and Execution

First, launch Docker daemon.

On macOS the daemon is started in the background together with `Docker Desktop` app.

On Ubuntu, launch the daemon with the following command:

```sh
sudo snap start docker
```

Specify the IP address and port on which MongoDB is listening

```sh
export MONGO_HOST=mongo
export MONGO_PORT=27017
```

Specify the URLs for the server and for the retrieval of access tokens (these are just examples)

```sh
export SERVER_URL='https://easypims.pimcity-h2020.eu/'
export TOKEN_URL='https://easypims.pimcity-h2020.eu/identity/auth/realms/pimcity/protocol/openid-connect/token'
export AUTH_URL='https://easypims.pimcity-h2020.eu/identity/auth/'
```

Set user and group IDs:

```sh
export DUID=$UID
export DGID=`id -g`
```

Set environment and debug mode on if needed (default is off):

```sh
export FLASK_ENV=development
export SERVER_DEBUG=True
```

Then, run the command below.

```sh
# Build and run dockerized Privacy Metrics
./build.sh
```

To stop the services use "CTRL+C" and run the command below.

```sh
# Stop and destroy containers
./stop_and_destroy.sh
```

### 2.3 Authorization

This module comes with a setup to integrated with PIMCity's EasyPIMS deployment which uses a on-premise [KeyCloak](https://www.keycloak.org/) instance as authentication provider.

For using Privacy Metrics in your environment with OAuth2.0 authorization enabled still using KeyCloak, you have to modify `tokenUrl` parameter in `swagger.yaml` the KeyCloak client configuration defined in `authorization_controller.py`.

Differently, to enable authorization workflow with other types of authentication providers, you must implement your own setup in `authorization_controller.py`.

## 3. Usage

### 3.1 Swagger UI

**NOTE:** Swagger UI is available in debug mode only (`SERVER_DEBUG=True`)

Open your browser on [http://localhost:8080/privacy-metrics/ui](http://localhost:8080/privacy-metrics/ui). This page provides the Swagger UI describing Privacy Metrics APIs, together with actions to activate and test APIs.

In order to test the APIs, you have to get authorization:
- on the UI page click on `Authorize`. In the modal `Available Authorizations` scroll down to `OAuth2 (OAuth2, clientCredentials)`, and fill the form with the client ID and secret that have been provided by the authorization provider administrator.
- select the scopes for which the client ID has been enabled.
- click on `Authorize` button.

Once obtained authorization to interact with APIs, Swagger UI will enable you to test all APIs allowed by the scope for the client ID in use.  

For a complete description of this process, check this [video](https://youtu.be/SdGuTt98JRg).

### 3.2 Generic client

Alternatively to Swagger UI, you can use whatever client software to test and check the APIs. See below.

**Check system health with cURL**

```sh
curl -X GET "http://localhost:8080/privacy-metrics/health" -H  "accept: */*"
```

Expected result:

```sh
"Everything's fine here! There are currently 9323 Privacy Metrics in the database at the moment"
```

**Get the authorization token**

To call Privacy Metrics APIs, you must obtain the credentials from the administrators of the authorization provider (KeyCloak in this case).

```sh
curl --location --request POST 'https://easypims.pimcity-h2020.eu/identity/auth/realms/pimcity/protocol/openid-connect/token' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'grant_type=client_credentials' \
--data-urlencode 'client_id=<YOUR_CLIENT_ID>' \
--data-urlencode 'client_secret=<YOUR_CLIENT_SECRET>'
```

The response to this request will contain the `access_token` to use in all subsequent requests:

```sh
{"access_token":"<YOUR_ACCESS_TOKEN>"}
```

From now on, all requests to APIs will have to attach the `access_token` to be authorized.

**Get the Privacy Metrics for the first 5 services in DB**

```sh
curl -X GET "http://localhost:8080/privacy-metrics/privacy-metrics?limit=5&offset=0" \
-H  'accept: application/json' \
-H 'Authorization: Bearer <YOUR_ACCESS_TOKEN>'
```

Example result:

```json
[
  [
    "alaska.edu",
    "3cb94130-a1eb-11eb-a48f-8c85904fb3aa"
  ],
  [
    "google.lk",
    "3caea27a-a1eb-11eb-a48f-8c85904fb3aa"
  ],
  [
    "jhu.edu",
    "3cad1d1a-a1eb-11eb-a48f-8c85904fb3aa"
  ],
  [
    "it.altervista.org",
    "3ca8dd0e-a1eb-11eb-a48f-8c85904fb3aa"
  ],
  [
    "ard.de",
    "3cad59e2-a1eb-11eb-a48f-8c85904fb3aa"
  ]
]
```

## 5. Development

Follow the steps listed in "Native deployment" and build the development setup as described below.

### 5.1 Dev Setup

First, run the following commands to activate hooks.

```sh
# Setup pre-commit and pre-push hooks
poetry run pre-commit install -t pre-commit
poetry run pre-commit install -t pre-push
```

### 5.2 Testing

To quickly run tests in the same environment you use for developing, you can directly invoke pytest:

```sh
# Install package in "editable" mode (plus dependencies).
# This is required because, as a python best practice,
# tests must always be executed against an installed package.
poetry install

# Run tests
poetry run pytest

# Run tests, with code coverage check
poetry run pytest --cov
```

### 5.3 Tox

Tox is configured to run both linting and tests in dedicated virtual environments. This ensures running tasks in a clean environment. Also, tox automatically creates a source distribution (`sdist`) of the python package and installs it before running tests.

```sh
# Run linting and tests with tox
poetry run tox

# Run linting and tests with tox, also recreating virtual environments from scratch
poetry run tox --recreate
```

## 6. Support

For any problem with this software, [file us an issue](https://gitlab.com/dashboard/issues?assignee_username=s.traverso), but please, **search first** to make sure no-one has reported the same issue already before opening one yourself.

## 7. Credits
This package was created with Cookiecutter.

## 8. Known issues

- In case the system complains the virtual env in broken:

  ```sh
  poetry config virtualenvs.in-project true
  ```

- In case you experience an error during the build such as 

  ```sh
  '~/.pyenv/versions/3.9.1/bin/python3.9' is not in the subpath of '<your-path>/privacy_metrics' OR one path is relative and the other is absolute.
  ```

  check whether there is a `.venv` folder in `src/` and  remove it.
