#!/bin/bash

set -e

echo "==== COPYING SEEDING DB TO MONGO_SEED ===="
cp seed/privacy_metrics.json Docker_build/mongo_seed/

pushd $(dirname $0) > /dev/null

rm -rf dist
mkdir -p dist

echo "==== BUILDING PYTHON PACKAGE ===="
bash build_python_package.sh

echo "==== BUILDING DOCKER IMAGES ===="
bash build_docker_images.sh

popd > /dev/null

echo "==== REMOVING SEEDING DB ===="
rm Docker_build/mongo_seed/privacy_metrics.json