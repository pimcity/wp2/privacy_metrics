#!/bin/bash

set -e

pushd $(dirname $0) > /dev/null

docker-compose up

popd > /dev/null