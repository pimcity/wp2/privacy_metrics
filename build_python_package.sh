#!/bin/bash

set -e

pushd $(dirname $0) > /dev/null

echo "==== BUILDING PYTHON PACKAGE ===="
poetry install
poetry build -f wheel

echo "==== EXPORTING LOCKED DEPENDENCIES FILE ===="
poetry export --without-hashes -f requirements.txt -o dist/requirements.txt

popd > /dev/null