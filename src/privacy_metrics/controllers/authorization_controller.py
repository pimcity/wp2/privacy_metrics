from privacy_metrics.core.utils import get_keycloak_conf

"""
controller generated to handled auth operation described at:
https://connexion.readthedocs.io/en/latest/security.html
"""


def check_OAuth2(token):

    keycloak_openid, KEYCLOAK_PUBLIC_KEY, options = get_keycloak_conf()

    token_info = keycloak_openid.decode_token(
        token, key=KEYCLOAK_PUBLIC_KEY, options=options
    )

    scope = token_info["scope"].split()

    return {"scopes": scope}


def validate_scope_OAuth2(required_scopes, token_scopes):
    return set(required_scopes).issubset(set(token_scopes))
