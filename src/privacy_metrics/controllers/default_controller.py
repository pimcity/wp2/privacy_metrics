from privacy_metrics.core import default


def health_get():  # noqa: E501
    """Checks if the server is running

     # noqa: E501


    :rtype: None
    """
    return default.check_health()
