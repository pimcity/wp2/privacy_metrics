import connexion

from privacy_metrics.core import privacy_metrics_db as privacy_metrics
from privacy_metrics.models.privacy_metric import PrivacyMetric  # noqa: E501
from privacy_metrics.models.provided_information import (  # noqa: E501
    ProvidedInformation,
)
from privacy_metrics.models.scores import Scores  # noqa: E501
from privacy_metrics.models.web_data import WebData  # noqa: E501


def create_privacy_metric(body):  # noqa: E501
    """Create a new Privacy Metric

    Creates a new privacy metric. This API is accessible in write mode exclusively to analytics generating PMs  # noqa: E501

    :param body:
    :type body: list | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        deserialized = [  # noqa: F841
            PrivacyMetric.from_dict(d) for d in connexion.request.get_json()
        ]  # noqa: E501
    return privacy_metrics.create_privacy_metric(body[0])


def delete_privacy_metric(name):  # noqa: E501
    """Deletes Privacy Metric

    Deletes a single Privacy Metrics based on the ID supplied. This API is available to analytics generating PMs only. # noqa: E501

    :param name: name of pm to delete
    :type name: string

    :rtype: None
    """
    return privacy_metrics.remove_privacy_metric(name)


def get_privacy_metric(name):  # noqa: E501
    """Get specific Privacy Metric

    Returns the Privacy Metric with specified ID. This API is accessible in read only mode to all authorized stakeholders  (users, data buyers and analytics generating PMs) # noqa: E501

    :param name: name of Privacy Metric to fetch
    :type name: string

    :rtype: PrivacyMetric
    """
    return privacy_metrics.get_privacy_metric(name)


def list_privacy_metrics(limit=None, offset=None):  # noqa: E501
    """Get services for which we have a Privacy Metric

    Obtain the list of services for which we have a Privacy Metrics. This API is accessible in read-only mode to all authorized stakeholders  (users, data buyers and analytics generating PMs) # noqa: E501

    :param limit: The number of items to return at one time
    :type limit: int
    :param offset: The number of items to skip before starting to collect the result set
    :type offset: int

    :rtype: PrivacyMetrics
    """
    return privacy_metrics.get_all_privacy_metrics(limit, offset)


def modify_provided_information(body, name):  # noqa: E501
    """Modifies specific Privacy Metric/Provided Information

    Updates Provided Information contained in Privacy Metric with properties to be changed. This API is available to data buyers only # noqa: E501

    :param body:
    :type body: dict | bytes
    :param name: name of Privacy Metric to update
    :type name: string

    :rtype: None
    """
    if connexion.request.is_json:
        deserialized = ProvidedInformation.from_dict(  # noqa: F841
            connexion.request.get_json()
        )  # noqa: E501
    token = None
    if "Authorization" in connexion.request.headers:
        token = connexion.request.headers["Authorization"].split(" ")[-1]
    return privacy_metrics.modify_part(body, name, "provided_information", token)


def modify_scores(body, name):  # noqa: E501
    """Modifies specific Privacy Metric/Scores

    Updates Scores contained in Privacy Metric with properties to be changed. This API is available to analytics only # noqa: E501

    :param body:
    :type body: dict | bytes
    :param name: name of Privacy Metric to update
    :type name: string

    :rtype: None
    """
    if connexion.request.is_json:
        deserialized = Scores.from_dict(  # noqa: F841
            connexion.request.get_json()
        )  # noqa: E501
    return privacy_metrics.modify_part(body, name, "scores", None)


def modify_webdata(body, name):  # noqa: E501
    """Modifies specific Privacy Metric/Webdata

    Updates Webdata contained in Privacy Metric with properties to be changed. This API is available to analytics only # noqa: E501

    :param body:
    :type body: dict | bytes
    :param name: name of Privacy Metric to update
    :type name: string

    :rtype: None
    """
    if connexion.request.is_json:
        deserialized = WebData.from_dict(  # noqa: F841
            connexion.request.get_json()
        )  # noqa: E501
    return privacy_metrics.update_part(body, name, "webdata", None)


def update_provided_information(body, name):  # noqa: E501
    """Substitutes specific Privacy Metric/Provided Information

    Substitutes the content of Provided Information contained in specific Privacy metric. This API is available to data buyers only # noqa: E501

    :param body:
    :type body: dict | bytes
    :param name: name of Privacy Metric to update
    :type name: string

    :rtype: None
    """
    if connexion.request.is_json:
        deserialized = ProvidedInformation.from_dict(  # noqa: F841
            connexion.request.get_json()
        )  # noqa: E501
    token = None
    if "Authorization" in connexion.request.headers:
        token = connexion.request.headers["Authorization"].split(" ")[-1]
    return privacy_metrics.update_part(body, name, "provided_information", token)


def update_scores(body, name):  # noqa: E501
    """Substitutes specific Privacy Metric/Scores

    Substitutes the content of Scores contained in specific Privacy metric. This API is available to analytics only # noqa: E501

    :param body:
    :type body: dict | bytes
    :param name: name of Privacy Metric to update
    :type name: string

    :rtype: None
    """
    if connexion.request.is_json:
        deserialized = Scores.from_dict(  # noqa: F841
            connexion.request.get_json()
        )  # noqa: E501
    return privacy_metrics.update_part(body, name, "scores", None)


def update_webdata(body, name):  # noqa: E501
    """Substitutes specific Privacy Metric/Webdata

    Substitutes the content of Webdata contained in specific Privacy metric. This API is available to analytics only # noqa: E501

    :param body:
    :type body: dict | bytes
    :param name: name of Privacy Metric to update
    :type name: string

    :rtype: None
    """
    if connexion.request.is_json:
        deserialized = WebData.from_dict(  # noqa: F841
            connexion.request.get_json()
        )  # noqa: E501
    return privacy_metrics.update_part(body, name, "webdata")
