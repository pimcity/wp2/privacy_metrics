from privacy_metrics.settings import MONGO_CLIENT, MONGO_COLLECTION_NAME, MONGO_DB_NAME


def check_health():
    collection = MONGO_CLIENT[MONGO_DB_NAME][MONGO_COLLECTION_NAME]

    return "Everything's fine here! There are {0} Privacy Metrics in the database at the moment".format(
        collection.count_documents(filter={})
    )
