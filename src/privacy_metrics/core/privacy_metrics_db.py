from privacy_metrics.core import scores, utils
from privacy_metrics.settings import MONGO_CLIENT, MONGO_COLLECTION_NAME, MONGO_DB_NAME


# Retrieves list of Privacy Metrics in DB with pagination options
# Limit: max number of PMs to return
# Offset: number of PMs to skip
def get_all_privacy_metrics(limit, offset):

    collection = MONGO_CLIENT[MONGO_DB_NAME][MONGO_COLLECTION_NAME]

    if not limit:
        return "Input validation failed", 400

    if not offset:
        offset = 0

    if limit * offset > collection.count_documents(filter={}):
        limit = collection.count_documents(filter={})
        offset = 0

    return [
        (
            pm["name"],
            pm["id"],
            pm["webdata"]["company_name"],
            pm["webdata"]["categories"],
            pm["scores"],
        )
        for pm in collection.find().skip(offset).limit(limit)
    ]


# Retrieves specific Privacy Metric
# Identifier: UUID of the item to return
def get_privacy_metric(name):

    collection = MONGO_CLIENT[MONGO_DB_NAME][MONGO_COLLECTION_NAME]

    # if not utils.is_valid_uuid(identifier, version=1):
    #     return "Input validation failed", 400

    pm = collection.find_one({"name": name}, {"_id": 0})
    if pm:
        return pm, 200

    return "Not found!", 404


# Updates scores any time Provided Information or Webdata are changed
def update_scores(updated_pm):
    collection = MONGO_CLIENT[MONGO_DB_NAME][MONGO_COLLECTION_NAME]

    name = updated_pm["name"]
    new_scores = scores.compute_scores(updated_pm)
    updated_scores = collection.find_one_and_update(
        {"name": name}, {"$set": {"scores": new_scores}}, upsert=True
    )
    return updated_scores


# Creates new Privacy Metric
# New: new privacy metrics to load in DB
# Returns identifier
def create_privacy_metric(body):

    collection = MONGO_CLIENT[MONGO_DB_NAME][MONGO_COLLECTION_NAME]

    if collection.find_one({"name": body["name"]}):
        return "Already exists!", 406

    body["identifier"] = utils.create_new_uuid()

    if collection.insert_one(body):
        pm_created = collection.find_one({"identifier": body["identifier"]}, {"_id": 0})
        update_scores(pm_created)
        return body["identifier"], 201

    return "Internal error.", 500


# Deletes specific Privacy Metric
# Identifier: UUID of the item to delete
# Returns the number of items left in DB
def remove_privacy_metric(name):

    collection = MONGO_CLIENT[MONGO_DB_NAME][MONGO_COLLECTION_NAME]

    if collection.find_one({"name": name}, {"_id": 0}):
        collection.delete_one({"name": name})
        return collection.count(), 200

    return "Not found!", 404


# Updates a whole part of Privacy Metric (Provided Information, Webdata or Scores)
# name: name of the item to modify
# Returns response code
def update_part(body, name, part, token):

    collection = MONGO_CLIENT[MONGO_DB_NAME][MONGO_COLLECTION_NAME]

    find_result = collection.find_one({"name": name}, {"_id": 0})
    if find_result:
        if part == "provided_information":
            user = None
            if token:
                user = utils.get_user_from(token)
            if not utils.check_owner(
                user, find_result["provided_information"]["owner"]
            ):
                return "Owner not authorized!", 401
            else:
                body["owner"] = user

        update_result = collection.find_one_and_update(
            {"name": name}, {"$set": {part: body}}, upsert=True
        )
        if update_result:
            update_result = collection.find_one({"name": name}, {"_id": 0})
            if part != "scores":
                update_scores(update_result)
            return "Replaced.", 200

    return "Not found!", 404


# Modifies specific field of Privacy Metric part (Provided Information, Webdata or Scores)
# name: name of the item to modify
# Returns response code
def modify_part(body, name, part, token):

    collection = MONGO_CLIENT[MONGO_DB_NAME][MONGO_COLLECTION_NAME]

    find_result = collection.find_one({"name": name}, {"_id": 0})
    if find_result:
        if part == "provided_information":
            user = None
            if token:
                user = utils.get_user_from(token)
            if not utils.check_owner(
                user, find_result["provided_information"]["owner"]
            ):
                return "Owner not authorized!", 401
            else:
                body["owner"] = user

        results = []
        for key, value in body.items():
            to_update = ".".join([part, key])
            result = collection.update_one(
                filter={"name": name}, update={"$set": {to_update: value}}, upsert=False
            )
            results.append(result)
        if all(results):
            update_result = collection.find_one({"name": name}, {"_id": 0})
            if part != "scores":
                update_scores(update_result)
            return "Modified.", 200

    return "Not found!", 404
