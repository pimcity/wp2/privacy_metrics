import pickle

from countrygroups import EUROPEAN_UNION

trackers = set()
sessionreplayers = set()
badsites = set()


def init_lists():
    global trackers
    global sessionreplayers
    global badsites

    # Load list with traking domains
    with open("seed/trackers.pkl", "rb") as fin:
        trackers = pickle.load(fin)
    # Load list with session replay scripts
    with open("seed/sessionreplayers.pkl", "rb") as fin:
        sessionreplayers = pickle.load(fin)
    # Load list with session replay scripts
    with open("seed/badsites.pkl", "rb") as fin:
        badsites = pickle.load(fin)


def data_controller_info_missing(pm, score, motivation):

    data_controller_contacts = [
        "data_controller_contact_address",
        "data_controller_contact_mail",
        "data_controller_contact_phone",
        "data_controller_identity",
    ]

    data_controller_contacts_missing = True
    for info in data_controller_contacts:
        # If at least one data controller contact field is provided, no penalization occurs
        if pm["provided_information"][info] and pm["provided_information"][
            info
        ] not in ["TBD", "NA"]:
            data_controller_contacts_missing = False

    if data_controller_contacts_missing:
        score -= 2.0
        motivation.add("Data controller identity and contacts are missing.")

    return score, motivation


def privacy_policy_missing(pm, score, motivation):

    if pm["webdata"]["privacy_policy"] is None:
        score -= 2.0
        motivation.add("Privacy policy is missing.")

    return score, motivation


def processing_purpose_missing(pm, score, motivation):
    # if pm["provided_information"]["data_processing_purposes"] is None or pm["provided_information"]["data_processing_purposes"] in ["TBD", "NA"]:

    return score, motivation


def gdpr_info_missing(pm, score, motivation):

    general_gdpr_info = [
        "consent_withdraw",
        "data_controller",
        "data_for_automatic_decision",
        "data_location",
        "data_processed",
        "data_processing_legal_basis",
        "data_processing_purposes",
        "data_processor",
        "data_recipients",
        "data_retention_period",
        "data_subprocessors",
        "data_transfer",
        "dpo_contact_address",
        "dpo_contact_mail",
        "dpo_contact_phone",
    ]

    for info in general_gdpr_info:
        if pm["provided_information"][info] is None or pm["provided_information"][
            info
        ] in ["TBD", "NA"]:
            score -= 0.5
            motivation.add("Important GDPR-related information is missing.")

    if pm["provided_information"]["user_rights"] is None or pm["provided_information"][
        "user_rights"
    ] in ["TBD", "NA"]:
        score -= 3.0
        motivation.add("Information about data access rights is missing.")

    return score, motivation


def tracking_services(pm, score, motivation):
    for third_party in pm["webdata"]["connected_third_parties"]:
        if third_party in trackers:
            score -= 0.05
            motivation.add("The service is associated to third-party tracking.")
        if third_party in sessionreplayers:
            score -= 0.1
            motivation.add("The service is associated to session replay scripts.")

    return score, motivation


def in_security_list(pm, score, motivation):
    if pm["name"] in badsites:
        score -= 4.0
        motivation.add("The service appears in some security list.")
    third_party_in_badsites = False
    for third_party in pm["webdata"]["connected_third_parties"]:
        if third_party in badsites:
            third_party_in_badsites = True

    if third_party_in_badsites:
        score -= 4.0
        motivation.add(
            "The service is associated to third parties appearing in some security list."
        )

    return score, motivation


def tracking_devices(pm, score, motivation):
    fingerprinting_techniques = [
        "fingerprinting:browser",
        "webgl",
        "canvas_font",
        "screen",
        "canvas",
        "plugins",
        "font",
    ]

    fingerprinting = False
    for tracking_device in pm["webdata"]["tracking_devices"]:
        if tracking_device in fingerprinting_techniques:
            fingerprinting = True

    if fingerprinting:
        motivation.add("The service uses fingerprinting techniques to track users.")
        score -= 2.0

    return score, motivation


def location_outside_EU(pm, score, motivation):
    location = pm["provided_information"]["data_location"]
    transfer = pm["provided_information"]["data_transfer"]

    outside_EU = False
    if location not in EUROPEAN_UNION.names:
        outside_EU = True

    if transfer not in EUROPEAN_UNION.names:
        outside_EU = True

    if outside_EU:
        motivation.add("Data location or transfer is outside EU.")
        score -= 2.0
    return score, motivation


def compute_privacy_score(pm):
    # TODO:
    # 1) check if PII is contained in cookies and/or local storage
    privacy_score = 5.0
    motivation = set()

    privacy_score, motivation = privacy_policy_missing(pm, privacy_score, motivation)
    privacy_score, motivation = data_controller_info_missing(
        pm, privacy_score, motivation
    )
    privacy_score, motivation = gdpr_info_missing(pm, privacy_score, motivation)
    privacy_score, motivation = tracking_services(pm, privacy_score, motivation)
    privacy_score, motivation = tracking_devices(pm, privacy_score, motivation)
    privacy_score, motivation = location_outside_EU(pm, privacy_score, motivation)
    privacy_score = max(0.0, privacy_score)

    return privacy_score, motivation


def compute_security_score(pm):
    # TODO:
    # 1) check security headers
    security_score = 5.0
    motivation = set()

    security_score, motivation = tracking_services(pm, security_score, motivation)
    security_score, motivation = tracking_devices(pm, security_score, motivation)
    security_score, motivation = in_security_list(pm, security_score, motivation)
    security_score = max(0.0, security_score)

    return security_score, motivation


def compute_transparency_score(pm):

    transparency_score = 5.0
    motivation = set()

    transparency_score, motivation = privacy_policy_missing(
        pm, transparency_score, motivation
    )
    transparency_score, motivation = data_controller_info_missing(
        pm, transparency_score, motivation
    )
    transparency_score, motivation = gdpr_info_missing(
        pm, transparency_score, motivation
    )
    transparency_score, motivation = in_security_list(
        pm, transparency_score, motivation
    )
    transparency_score = max(0.0, transparency_score)

    return transparency_score, motivation


def compute_scores(pm):

    privacy_score, privacy_motivation = compute_privacy_score(pm)
    security_score, security_motivation = compute_security_score(pm)
    transparency_score, transparency_motivation = compute_transparency_score(pm)

    scores = {
        "privacy_score": privacy_score,
        "security_score": security_score,
        "transparency_score": transparency_score,
        "privacy_motivation": list(privacy_motivation),
        "security_motivation": list(security_motivation),
        "transparency_motivation": list(transparency_motivation),
    }

    return scores
