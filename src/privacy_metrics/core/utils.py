import traceback
import uuid

import jsonschema
from keycloak import KeycloakOpenID

from privacy_metrics.settings import AUTH_URL


def get_keycloak_conf():
    """
    Returns KeyCloak configuration

    Parameters
    ----------
    None

    Returns
    -------
    keycloakopenid
    KEYCLOAK_PUBLIC_KEY
    options
    """

    keycloak_openid = KeycloakOpenID(
        server_url=AUTH_URL,
        client_id="ppm",
        realm_name="pimcity",
    )

    KEYCLOAK_PUBLIC_KEY = (
        "-----BEGIN PUBLIC KEY-----\n"
        + keycloak_openid.public_key()
        + "\n-----END PUBLIC KEY-----"
    )
    options = {
        "verify_signature": True,
        "verify_aud": True,
        "verify_exp": True,
        "audience": "ppm",
    }

    return keycloak_openid, KEYCLOAK_PUBLIC_KEY, options


def get_user_from(token):
    """
    Extracts clientId or email from token

    Parameters
    ----------
    token : str
    version : {1, 2, 3, 4}

    Returns
    -------
    user
    """

    if token is None:
        return None

    keycloak_openid, KEYCLOAK_PUBLIC_KEY, options = get_keycloak_conf()

    token_info = keycloak_openid.decode_token(
        token, key=KEYCLOAK_PUBLIC_KEY, options=options
    )

    user = None
    if "email" in token_info:
        user = token_info["email"]
    elif "clientId" in token_info:
        user = token_info["clientId"]

    return user


def check_owner(owner_in_body, owner_in_db):
    """
    Check if owner_in_body is compatible with owner_in_db

    Parameters
    ----------
    uuid_to_test : str
    version : {1, 2, 3, 4}

     Returns
    -------
    `True` if owner_in_db is not TBP AND if owner_in_body corresponds to owner_in_db.
    `True` if owner_in_db is TBP
    `False` otherwise
    """

    if owner_in_db == "TBP":
        return True
    if owner_in_db != "TBP" and owner_in_body == owner_in_db:
        return True

    return False


def create_new_uuid():
    return str(uuid.uuid1())


def is_valid_uuid(uuid_to_test, version=4):
    """
    Check if uuid_to_test is a valid UUID.

     Parameters
    ----------
    uuid_to_test : str
    version : {1, 2, 3, 4}

     Returns
    -------
    `True` if uuid_to_test is a valid UUID, otherwise `False`.
    """

    try:
        uuid_obj = uuid.UUID(uuid_to_test, version=version)
    except ValueError:
        return False
    return str(uuid_obj) == uuid_to_test


def validateJson(data, schema):
    try:
        jsonschema.validate(instance=data, schema=schema)
    except Exception:
        print(traceback.format_exc())
        return False
    return True
