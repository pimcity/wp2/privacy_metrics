import connexion
from flask_cors import CORS
from pyaml_env import parse_config

from privacy_metrics import encoder
from privacy_metrics.settings import DEBUG, PORT, SWAGGER_UI


def main():
    app = connexion.App(
        __name__, specification_dir="./swagger/", options={"swagger_ui": SWAGGER_UI}
    )
    CORS(app.app)

    swagger_config = parse_config(str(app.get_root_path()) + "/swagger/swagger.yaml")

    app.app.json_encoder = encoder.JSONEncoder
    app.add_api(
        swagger_config,
        arguments={"title": "PIMCity Privacy Metrics"},
        strict_validation=True,
        pythonic_params=True,
    )
    app.run(port=PORT, debug=DEBUG)


if __name__ == "__main__":
    main()
