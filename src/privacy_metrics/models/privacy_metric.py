# coding: utf-8

from __future__ import absolute_import

from datetime import date, datetime  # noqa: F401
from typing import Dict, List  # noqa: F401

from privacy_metrics import util
from privacy_metrics.models.base_model_ import Model
from privacy_metrics.models.provided_information import (  # noqa: F401,E501
    ProvidedInformation,
)
from privacy_metrics.models.scores import Scores  # noqa: F401,E501
from privacy_metrics.models.web_data import WebData  # noqa: F401,E501


class PrivacyMetric(Model):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    def __init__(
        self,
        identifier: str = None,
        name: str = None,
        provided_information: ProvidedInformation = None,
        webdata: WebData = None,
        scores: Scores = None,
    ):  # noqa: E501
        """PrivacyMetric - a model defined in Swagger

        :param identifier: The id of this PrivacyMetric.  # noqa: E501
        :type identifier: str
        :param name: The name of this PrivacyMetric.  # noqa: E501
        :type name: str
        :param provided_information: The provided_information of this PrivacyMetric.  # noqa: E501
        :type provided_information: ProvidedInformation
        :param webdata: The webdata of this PrivacyMetric.  # noqa: E501
        :type webdata: WebData
        :param scores: The scores of this PrivacyMetric.  # noqa: E501
        :type scores: Scores
        """
        self.swagger_types = {
            "identifier": str,
            "name": str,
            "provided_information": ProvidedInformation,
            "webdata": WebData,
            "scores": Scores,
        }

        self.attribute_map = {
            "identifier": "identifier",
            "name": "name",
            "provided_information": "provided_information",
            "webdata": "webdata",
            "scores": "scores",
        }
        self._identifier = identifier
        self._name = name
        self._provided_information = provided_information
        self._webdata = webdata
        self._scores = scores

    @classmethod
    def from_dict(cls, dikt) -> "PrivacyMetric":
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The PrivacyMetric of this PrivacyMetric.  # noqa: E501
        :rtype: PrivacyMetric
        """
        return util.deserialize_model(dikt, cls)

    @property
    def identifier(self) -> str:
        """Gets the id of this PrivacyMetric.


        :return: The id of this PrivacyMetric.
        :rtype: str
        """
        return self._identifier

    @identifier.setter
    def identifier(self, identifier: str):
        """Sets the id of this PrivacyMetric.


        :param identifier: The id of this PrivacyMetric.
        :type identifier: str
        """
        if identifier is None:
            raise ValueError(
                "Invalid value for `identifier`, must not be `None`"
            )  # noqa: E501

        self._identifier = identifier

    @property
    def name(self) -> str:
        """Gets the name of this PrivacyMetric.


        :return: The name of this PrivacyMetric.
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name: str):
        """Sets the name of this PrivacyMetric.


        :param name: The name of this PrivacyMetric.
        :type name: str
        """
        if name is None:
            raise ValueError(
                "Invalid value for `name`, must not be `None`"
            )  # noqa: E501

        self._name = name

    @property
    def provided_information(self) -> ProvidedInformation:
        """Gets the provided_information of this PrivacyMetric.


        :return: The provided_information of this PrivacyMetric.
        :rtype: ProvidedInformation
        """
        return self._provided_information

    @provided_information.setter
    def provided_information(self, provided_information: ProvidedInformation):
        """Sets the provided_information of this PrivacyMetric.


        :param provided_information: The provided_information of this PrivacyMetric.
        :type provided_information: ProvidedInformation
        """
        if provided_information is None:
            raise ValueError(
                "Invalid value for `provided_information`, must not be `None`"
            )  # noqa: E501

        self._provided_information = provided_information

    @property
    def webdata(self) -> WebData:
        """Gets the webdata of this PrivacyMetric.


        :return: The webdata of this PrivacyMetric.
        :rtype: WebData
        """
        return self._webdata

    @webdata.setter
    def webdata(self, webdata: WebData):
        """Sets the webdata of this PrivacyMetric.


        :param webdata: The webdata of this PrivacyMetric.
        :type webdata: WebData
        """
        if webdata is None:
            raise ValueError(
                "Invalid value for `webdata`, must not be `None`"
            )  # noqa: E501

        self._webdata = webdata

    @property
    def scores(self) -> Scores:
        """Gets the scores of this PrivacyMetric.


        :return: The scores of this PrivacyMetric.
        :rtype: Scores
        """
        return self._scores

    @scores.setter
    def scores(self, scores: Scores):
        """Sets the scores of this PrivacyMetric.


        :param scores: The scores of this PrivacyMetric.
        :type scores: Scores
        """
        if scores is None:
            raise ValueError(
                "Invalid value for `scores`, must not be `None`"
            )  # noqa: E501

        self._scores = scores
