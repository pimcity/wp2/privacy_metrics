# coding: utf-8

from __future__ import absolute_import

from datetime import date, datetime  # noqa: F401
from typing import Dict, List  # noqa: F401

from privacy_metrics import util
from privacy_metrics.models.base_model_ import Model


class Scores(Model):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    def __init__(
        self,
        privacy_score: float = None,
        security_score: float = None,
        transparency_score: float = None,
        privacy_motivation: List[str] = None,
        security_motivation: List[str] = None,
        transparency_motivation: List[str] = None,
    ):  # noqa: E501
        """Scores - a model defined in Swagger

        :param privacy_score: The privacy_score of this Scores.  # noqa: E501
        :type privacy_score: float
        :param security_score: The security_score of this Scores.  # noqa: E501
        :type security_score: float
        :param transparency_score: The transparency_score of this Scores.  # noqa: E501
        :type transparency_score: float
        :param privacy_motivation: The privacy_motivation of this Scores.  # noqa: E501
        :type privacy_motivation: List[str]
        :param security_motivation: The security_motivation of this Scores.  # noqa: E501
        :type security_motivation: List[str]
        :param transparency_motivation: The transparency_motivation of this Scores.  # noqa: E501
        :type transparency_motivation: List[str]
        """
        self.swagger_types = {
            "privacy_score": float,
            "security_score": float,
            "transparency_score": float,
            "privacy_motivation": List[str],
            "security_motivation": List[str],
            "transparency_motivation": List[str],
        }

        self.attribute_map = {
            "privacy_score": "privacy_score",
            "security_score": "security_score",
            "transparency_score": "transparency_score",
            "privacy_motivation": "privacy_motivation",
            "security_motivation": "security_motivation",
            "transparency_motivation": "transparency_motivation",
        }
        self._privacy_score = privacy_score
        self._security_score = security_score
        self._transparency_score = transparency_score
        self._privacy_motivation = privacy_motivation
        self._security_motivation = security_motivation
        self._transparency_motivation = transparency_motivation

    @classmethod
    def from_dict(cls, dikt) -> "Scores":
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The Scores of this Scores.  # noqa: E501
        :rtype: Scores
        """
        return util.deserialize_model(dikt, cls)

    @property
    def privacy_score(self) -> float:
        """Gets the privacy_score of this Scores.


        :return: The privacy_score of this Scores.
        :rtype: int
        """
        return self._privacy_score

    @privacy_score.setter
    def privacy_score(self, privacy_score: float):
        """Sets the privacy_score of this Scores.


        :param privacy_score: The privacy_score of this Scores.
        :type privacy_score: float
        """
        if privacy_score is None:
            raise ValueError(
                "Invalid value for `privacy_score`, must not be `None`"
            )  # noqa: E501

        self._privacy_score = privacy_score

    @property
    def security_score(self) -> float:
        """Gets the security_score of this Scores.


        :return: The security_score of this Scores.
        :rtype: float
        """
        return self._security_score

    @security_score.setter
    def security_score(self, security_score: float):
        """Sets the security_score of this Scores.


        :param security_score: The security_score of this Scores.
        :type security_score: float
        """
        if security_score is None:
            raise ValueError(
                "Invalid value for `security_score`, must not be `None`"
            )  # noqa: E501

        self._security_score = security_score

    @property
    def transparency_score(self) -> float:
        """Gets the transparency_score of this Scores.


        :return: The transparency_score of this Scores.
        :rtype: float
        """
        return self._transparency_score

    @transparency_score.setter
    def transparency_score(self, transparency_score: float):
        """Sets the transparency_score of this Scores.


        :param transparency_score: The transparency_score of this Scores.
        :type transparency_score: float
        """
        if transparency_score is None:
            raise ValueError(
                "Invalid value for `transparency_score`, must not be `None`"
            )  # noqa: E501

        self._transparency_score = transparency_score

    @property
    def privacy_motivation(self) -> List[str]:
        """Gets the privacy_motivation of this Scores.


        :return: The privacy_motivation of this Scores.
        :rtype: List[str]
        """
        return self._privacy_motivation

    @privacy_motivation.setter
    def privacy_motivation(self, privacy_motivation: List[str]):
        """Sets the privacy_motivation of this Scores.


        :param privacy_motivation: The privacy_motivation of this Scores.
        :type privacy_motivation: List[str]
        """

        self._privacy_motivation = privacy_motivation

    @property
    def security_motivation(self) -> List[str]:
        """Gets the security_motivation of this Scores.


        :return: The security_motivation of this Scores.
        :rtype: List[str]
        """
        return self._security_motivation

    @security_motivation.setter
    def security_motivation(self, security_motivation: List[str]):
        """Sets the security_motivation of this Scores.


        :param security_motivation: The security_motivation of this Scores.
        :type security_motivation: List[str]
        """

        self._security_motivation = security_motivation

    @property
    def transparency_motivation(self) -> List[str]:
        """Gets the transparency_motivation of this Scores.


        :return: The transparency_motivation of this Scores.
        :rtype: List[str]
        """
        return self._transparency_motivation

    @transparency_motivation.setter
    def transparency_motivation(self, transparency_motivation: List[str]):
        """Sets the transparency_motivation of this Scores.


        :param transparency_motivation: The transparency_motivation of this Scores.
        :type transparency_motivation: List[str]
        """

        self._transparency_motivation = transparency_motivation
