# coding: utf-8

from __future__ import absolute_import

from datetime import date, datetime  # noqa: F401
from typing import Dict, List  # noqa: F401

from privacy_metrics import util
from privacy_metrics.models.base_model_ import Model


class WebData(Model):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    def __init__(
        self,
        company_name: str = None,
        operates_under: str = None,
        categories: List[str] = None,
        rank_in_category: int = None,
        connected_third_parties: List[str] = None,
        connected_websites: List[str] = None,
        tracking_devices: List[str] = None,
        privacy_policy: str = None,
        presence_in_security_lists: List[str] = None,
        website: bool = None,
        third_party: bool = None,
        reach: int = None,
    ):  # noqa: E501
        """WebData - a model defined in Swagger

        :param company_name: The company_name of this WebData.  # noqa: E501
        :type company_name: str
        :param operates_under: The operates_under of this WebData.  # noqa: E501
        :type operates_under: str
        :param categories: The categories of this WebData.  # noqa: E501
        :type categories: List[str]
        :param rank_in_category: The rank_in_category of this WebData.  # noqa: E501
        :type rank_in_category: int
        :param connected_third_parties: The connected_third_parties of this WebData.  # noqa: E501
        :type connected_third_parties: List[str]
        :param connected_websites: The connected_websites of this WebData.  # noqa: E501
        :type connected_websites: List[str]
        :param tracking_devices: The tracking_devices of this WebData.  # noqa: E501
        :type tracking_devices: List[str]
        :param privacy_policy: The privacy_policy of this WebData.  # noqa: E501
        :type privacy_policy: str
        :param presence_in_security_lists: The presence_in_security_lists of this WebData.  # noqa: E501
        :type presence_in_security_lists: List[str]
        :param website: The website of this WebData.  # noqa: E501
        :type website: bool
        :param third_party: The third_party of this WebData.  # noqa: E501
        :type third_party: bool
        :param reach: The reach of this WebData.  # noqa: E501
        :type reach: int
        """
        self.swagger_types = {
            "company_name": str,
            "operates_under": str,
            "categories": List[str],
            "rank_in_category": int,
            "connected_third_parties": List[str],
            "connected_websites": List[str],
            "tracking_devices": List[str],
            "privacy_policy": str,
            "presence_in_security_lists": List[str],
            "website": bool,
            "third_party": bool,
            "reach": int,
        }

        self.attribute_map = {
            "company_name": "company_name",
            "operates_under": "operates_under",
            "categories": "categories",
            "rank_in_category": "rank_in_category",
            "connected_third_parties": "connected_third_parties",
            "connected_websites": "connected_websites",
            "tracking_devices": "tracking_devices",
            "privacy_policy": "privacy_policy",
            "presence_in_security_lists": "presence_in_security_lists",
            "website": "website",
            "third_party": "third_party",
            "reach": "reach",
        }
        self._company_name = company_name
        self._operates_under = operates_under
        self._categories = categories
        self._rank_in_category = rank_in_category
        self._connected_third_parties = connected_third_parties
        self._connected_websites = connected_websites
        self._tracking_devices = tracking_devices
        self._privacy_policy = privacy_policy
        self._presence_in_security_lists = presence_in_security_lists
        self._website = website
        self._third_party = third_party
        self._reach = reach

    @classmethod
    def from_dict(cls, dikt) -> "WebData":
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The WebData of this WebData.  # noqa: E501
        :rtype: WebData
        """
        return util.deserialize_model(dikt, cls)

    @property
    def company_name(self) -> str:
        """Gets the company_name of this WebData.


        :return: The company_name of this WebData.
        :rtype: str
        """
        return self._company_name

    @company_name.setter
    def company_name(self, company_name: str):
        """Sets the company_name of this WebData.


        :param company_name: The company_name of this WebData.
        :type company_name: str
        """
        if company_name is None:
            raise ValueError(
                "Invalid value for `company_name`, must not be `None`"
            )  # noqa: E501

        self._company_name = company_name

    @property
    def operates_under(self) -> str:
        """Gets the operates_under of this WebData.


        :return: The operates_under of this WebData.
        :rtype: str
        """
        return self._operates_under

    @operates_under.setter
    def operates_under(self, operates_under: str):
        """Sets the operates_under of this WebData.


        :param operates_under: The operates_under of this WebData.
        :type operates_under: str
        """

        self._operates_under = operates_under

    @property
    def categories(self) -> List[str]:
        """Gets the categories of this WebData.


        :return: The categories of this WebData.
        :rtype: List[str]
        """
        return self._categories

    @categories.setter
    def categories(self, categories: List[str]):
        """Sets the categories of this WebData.


        :param categories: The categories of this WebData.
        :type categories: List[str]
        """

        self._categories = categories

    @property
    def rank_in_category(self) -> int:
        """Gets the rank_in_category of this WebData.


        :return: The rank_in_category of this WebData.
        :rtype: int
        """
        return self._rank_in_category

    @rank_in_category.setter
    def rank_in_category(self, rank_in_category: int):
        """Sets the rank_in_category of this WebData.


        :param rank_in_category: The rank_in_category of this WebData.
        :type rank_in_category: int
        """

        self._rank_in_category = rank_in_category

    @property
    def connected_third_parties(self) -> List[str]:
        """Gets the connected_third_parties of this WebData.


        :return: The connected_third_parties of this WebData.
        :rtype: List[str]
        """
        return self._connected_third_parties

    @connected_third_parties.setter
    def connected_third_parties(self, connected_third_parties: List[str]):
        """Sets the connected_third_parties of this WebData.


        :param connected_third_parties: The connected_third_parties of this WebData.
        :type connected_third_parties: List[str]
        """

        self._connected_third_parties = connected_third_parties

    @property
    def connected_websites(self) -> List[str]:
        """Gets the connected_websites of this WebData.


        :return: The connected_websites of this WebData.
        :rtype: List[str]
        """
        return self._connected_websites

    @connected_websites.setter
    def connected_websites(self, connected_websites: List[str]):
        """Sets the connected_websites of this WebData.


        :param connected_websites: The connected_websites of this WebData.
        :type connected_websites: List[str]
        """

        self._connected_websites = connected_websites

    @property
    def tracking_devices(self) -> List[str]:
        """Gets the tracking_devices of this WebData.


        :return: The tracking_devices of this WebData.
        :rtype: List[str]
        """
        return self._tracking_devices

    @tracking_devices.setter
    def tracking_devices(self, tracking_devices: List[str]):
        """Sets the tracking_devices of this WebData.


        :param tracking_devices: The tracking_devices of this WebData.
        :type tracking_devices: List[str]
        """

        self._tracking_devices = tracking_devices

    @property
    def privacy_policy(self) -> str:
        """Gets the privacy_policy of this WebData.


        :return: The privacy_policy of this WebData.
        :rtype: str
        """
        return self._privacy_policy

    @privacy_policy.setter
    def privacy_policy(self, privacy_policy: str):
        """Sets the privacy_policy of this WebData.


        :param privacy_policy: The privacy_policy of this WebData.
        :type privacy_policy: str
        """

        self._privacy_policy = privacy_policy

    @property
    def presence_in_security_lists(self) -> List[str]:
        """Gets the presence_in_security_lists of this WebData.


        :return: The presence_in_security_lists of this WebData.
        :rtype: List[str]
        """
        return self._presence_in_security_lists

    @presence_in_security_lists.setter
    def presence_in_security_lists(self, presence_in_security_lists: List[str]):
        """Sets the presence_in_security_lists of this WebData.


        :param presence_in_security_lists: The presence_in_security_lists of this WebData.
        :type presence_in_security_lists: List[str]
        """

        self._presence_in_security_lists = presence_in_security_lists

    @property
    def website(self) -> bool:
        """Gets the website of this WebData.


        :return: The website of this WebData.
        :rtype: bool
        """
        return self._website

    @website.setter
    def website(self, website: bool):
        """Sets the website of this WebData.


        :param website: The website of this WebData.
        :type website: bool
        """

        self._website = website

    @property
    def third_party(self) -> bool:
        """Gets the third_party of this WebData.


        :return: The third_party of this WebData.
        :rtype: bool
        """
        return self._third_party

    @third_party.setter
    def third_party(self, third_party: bool):
        """Sets the third_party of this WebData.


        :param third_party: The third_party of this WebData.
        :type third_party: bool
        """

        self._third_party = third_party

    @property
    def reach(self) -> int:
        """Gets the reach of this WebData.


        :return: The reach of this WebData.
        :rtype: int
        """
        return self._reach

    @reach.setter
    def reach(self, reach: int):
        """Sets the reach of this WebData.


        :param reach: The reach of this WebData.
        :type reach: int
        """

        self._reach = reach
