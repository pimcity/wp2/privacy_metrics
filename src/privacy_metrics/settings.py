import os

from pymongo import MongoClient

PORT = 8080
DEBUG = os.getenv("SERVER_DEBUG", "False").lower() in ("true", "1", "t")
SWAGGER_UI = DEBUG
DEFAULT_PAGE_LIMIT = 5
MONGO_HOST = os.getenv("MONGO_HOST") if os.getenv("MONGO_HOST") else "localhost"
MONGO_PORT = int(os.getenv("MONGO_PORT")) if os.getenv("MONGO_PORT") else 27017
MONGO_DB_NAME = "testdb"
MONGO_COLLECTION_NAME = "privacy_metrics"
MONGO_CLIENT = MongoClient(host=MONGO_HOST, port=MONGO_PORT)
AUTH_URL = os.getenv("AUTH_URL") if os.getenv("AUTH_URL") else "localhost"
