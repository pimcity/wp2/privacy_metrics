openapi: 3.0.1
info:
  title: PIMCity Privacy Metrics
  description: "The Privacy Metric API allows EasyPIMS' users to discover and search\
    \ services' and data buyers' Privacy Metrics. \n- `GET /privacy-metrics` allows\
    \ **all stakeholders** to obtain the list of services for which Privacy Metrics\
    \ are available.\n- `POST /privacy-metrics` allows **the analytics** to create\
    \ a new Privacy Metric.\n- `GET /privacy-metrics/{identifier}` allows **all stakeholders**\
    \ to obtain the Privacy Metric corresponding to `id`\n- `DELETE /privacy-metrics/{identifier}`\
    \ allows **the analytics** to delete the Privacy Metric corresponding to `id`\n\
    - `PUT|PATCH /privacy-metrics/{identifier}/provided-information` allows **the Data Buyer**\
    \ to substitutes|modify its Provided Information\n- `PUT|PATCH /privacy-metrics/{identifier}/webdata`\
    \ allows **the analytics** to substitutes|modify webdata corresponding to Privacy\
    \ Metric `id`\n- `PUT|PATCH /privacy-metrics/{identifier}/scores` allows **the analytics**\
    \ to substitutes|modify scores corresponding to Privacy Metric `id`"
  contact:
    name: PIMCity
    url: https://www.pimcity.eu
    email: s.traverso@ermes.company
  version: 0.0.1
servers:
- url: !ENV ${SERVER_URL}
tags:
- name: metadata
  description: Find out about the data sets
- name: search
  description: Search a data set
paths:
  /health:
    get:
      summary: Checks if the server is running
      operationId: health_get
      responses:
        "200":
          description: Server is up and running
        default:
          description: Something went wrong
      x-openapi-router-controller: privacy_metrics.controllers.default_controller
  /privacy-metrics:
    get:
      tags:
      - privacy-metrics
      summary: Get services for which we have a Privacy Metric
      description: "Obtain the list of services for which we have a Privacy Metrics.\
        \ This API is accessible in read-only mode to all authorized stakeholders\
        \  (users, data buyers and analytics generating PMs)"
      operationId: list_privacy_metrics
      parameters:
      - name: limit
        in: query
        description: The number of items to return at one time
        required: false
        style: form
        explode: true
        schema:
          maximum: 50
          minimum: 0
          type: integer
          format: int32
          default: 25
      - name: offset
        in: query
        description: The number of items to skip before starting to collect the result
          set
        required: false
        style: form
        explode: true
        schema:
          minimum: 0
          type: integer
          format: int32
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PrivacyMetrics'
        default:
          description: Unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
      security:
      - OAuth2:
        - read:privacy-metrics
      x-openapi-router-controller: privacy_metrics.controllers.privacy_metrics_controller
    post:
      tags:
      - privacy-metrics
      summary: Create a new Privacy Metric
      description: 'Creates a new privacy metric. This API is accessible in write
        mode exclusively to analytics generating PMs '
      operationId: create_privacy_metric
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/PrivacyMetrics'
        required: true
      responses:
        "201":
          description: Created
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
      security:
      - OAuth2:
        - create:privacy-metric
      x-openapi-router-controller: privacy_metrics.controllers.privacy_metrics_controller
  /privacy-metrics/{name}/:
    get:
      tags:
      - privacy-metrics
      summary: Get specific Privacy Metric
      description: "Returns the Privacy Metric with specified Name. This API is accessible\
        \ in read only mode to all authorized stakeholders  (users, data buyers and\
        \ analytics generating PMs)"
      operationId: get_privacy_metric
      parameters:
      - name: name
        in: path
        description: Name of Privacy Metric to fetch
        required: true
        style: simple
        explode: false
        schema:
          type: string
      responses:
        "200":
          description: Privacy Metric response
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PrivacyMetric'
        "400":
          description: Bad request. ID must be uuid.
        "401":
          description: Authorization information is missing or invalid.
        "404":
          description: A Privacy Metric with the specified ID was not found.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "5XX":
          description: Unexpected error.
        default:
          description: Unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
      security:
      - OAuth2:
        - read:privacy-metrics
      x-openapi-router-controller: privacy_metrics.controllers.privacy_metrics_controller
    delete:
      tags:
      - privacy-metrics
      summary: Deletes Privacy Metric
      description: Deletes a single Privacy Metrics based on the name supplied. This
        API is available to analytics generating PMs only.
      operationId: delete_privacy_metric
      parameters:
      - name: name
        in: path
        description: Name of Privacy Metric to delete
        required: true
        style: simple
        explode: false
        schema:
          type: string
      responses:
        "200":
          description: Deleted
        "400":
          description: Bad request. ID must be uuid.
        "401":
          description: Authorization information is missing or invalid.
        "404":
          description: A Privacy Metric with the specified ID was not found.
        "5XX":
          description: Unexpected error.
        default:
          description: Unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
      security:
      - OAuth2:
        - delete:privacy-metric
      x-openapi-router-controller: privacy_metrics.controllers.privacy_metrics_controller
  /privacy-metrics/{name}/provided-information:
    put:
      tags:
      - privacy-metrics
      summary: Substitutes specific Privacy Metric/Provided Information
      description: Substitutes the content of Provided Information contained in specific
        Privacy metric. This API is available to data buyers only
      operationId: update_provided_information
      parameters:
      - name: name
        in: path
        description: Name of Privacy Metric to update
        required: true
        style: simple
        explode: false
        schema:
          type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ProvidedInformation'
        required: true
      responses:
        "200":
          description: Replaced
        "400":
          description: Bad request. Name must be a string.
        "401":
          description: Authorization information is missing or invalid.
        "404":
          description: A Privacy Metric with the specified ID was not found.
        "5XX":
          description: Unexpected error.
        default:
          description: Unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
      security:
      - OAuth2:
        - update:provided-information
      x-openapi-router-controller: privacy_metrics.controllers.privacy_metrics_controller
    patch:
      tags:
      - privacy-metrics
      summary: Modifies specific Privacy Metric/Provided Information
      description: Updates Provided Information contained in Privacy Metric with properties
        to be changed. This API is available to data buyers only
      operationId: modify_provided_information
      parameters:
      - name: name
        in: path
        description: name of Privacy Metric to update
        required: true
        style: simple
        explode: false
        schema:
          type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UpdateProvidedInformation'
        required: true
      responses:
        "204":
          description: Updated
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
      security:
      - OAuth2:
        - update:provided-information
      x-openapi-router-controller: privacy_metrics.controllers.privacy_metrics_controller
  /privacy-metrics/{name}/webdata:
    put:
      tags:
      - privacy-metrics
      summary: Substitutes specific Privacy Metric/Webdata
      description: Substitutes the content of Webdata contained in specific Privacy
        metric. This API is available to analytics only
      operationId: update_webdata
      parameters:
      - name: name
        in: path
        description: name of Privacy Metric to update
        required: true
        style: simple
        explode: false
        schema:
          type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/WebData'
        required: true
      responses:
        "200":
          description: Replaced
        "400":
          description: Bad request. name must be string.
        "401":
          description: Authorization information is missing or invalid.
        "404":
          description: A Privacy Metric with the specified name was not found.
        "5XX":
          description: Unexpected error.
        default:
          description: Unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
      security:
      - OAuth2:
        - update:webdata
      x-openapi-router-controller: privacy_metrics.controllers.privacy_metrics_controller
    patch:
      tags:
      - privacy-metrics
      summary: Modifies specific Privacy Metric/Webdata
      description: Updates Webdata contained in Privacy Metric with properties to
        be changed. This API is available to analytics only
      operationId: modify_webdata
      parameters:
      - name: name
        in: path
        description: name of Privacy Metric to update
        required: true
        style: simple
        explode: false
        schema:
          type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UpdateWebData'
        required: true
      responses:
        "204":
          description: Updated
        "400":
          description: Bad request. ID must be uuid.
        "401":
          description: Authorization information is missing or invalid.
        "404":
          description: A Privacy Metric with the specified ID was not found.
        "5XX":
          description: Unexpected error.
        default:
          description: Unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
      security:
      - OAuth2:
        - update:webdata
      x-openapi-router-controller: privacy_metrics.controllers.privacy_metrics_controller
  /privacy-metrics/{name}/scores:
    put:
      tags:
      - privacy-metrics
      summary: Substitutes specific Privacy Metric/Scores
      description: Substitutes the content of Scores contained in specific Privacy
        metric. This API is available to analytics only
      operationId: update_scores
      parameters:
      - name: name
        in: path
        description: name of Privacy Metric to update
        required: true
        style: simple
        explode: false
        schema:
          type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Scores'
        required: true
      responses:
        "200":
          description: Replaced
        "400":
          description: Bad request. ID must be uuid.
        "401":
          description: Authorization information is missing or invalid.
        "404":
          description: A Privacy Metric with the specified ID was not found.
        "5XX":
          description: Unexpected error.
        default:
          description: Unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
      security:
      - OAuth2:
        - update:scores
      x-openapi-router-controller: privacy_metrics.controllers.privacy_metrics_controller
    patch:
      tags:
      - privacy-metrics
      summary: Modifies specific Privacy Metric/Scores
      description: Updates Scores contained in Privacy Metric with properties to be
        changed. This API is available to analytics only
      operationId: modify_scores
      parameters:
      - name: name
        in: path
        description: name of Privacy Metric to update
        required: true
        style: simple
        explode: false
        schema:
          type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UpdateScores'
        required: true
      responses:
        "200":
          description: Updated
        "400":
          description: Bad request. ID must be uuid.
        "401":
          description: Authorization information is missing or invalid.
        "404":
          description: A Privacy Metric with the specified ID was not found.
        "5XX":
          description: Unexpected error.
        default:
          description: Unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
      security:
      - OAuth2:
        - update:scores
      x-openapi-router-controller: privacy_metrics.controllers.privacy_metrics_controller
components:
  schemas:
    ProvidedInformation:
      type: object
      allOf:
        - $ref: '#/components/schemas/ProvidedInformationProperties'
        - required:
          - owner
          - category
          - consent_withdraw
          - country
          - data_controller
          - data_controller_contact_address
          - data_controller_contact_mail
          - data_controller_contact_phone
          - data_controller_identity
          - data_for_automatic_decision
          - data_location
          - data_processed
          - data_processing_legal_basis
          - data_processing_purposes
          - data_processor
          - data_recipients
          - data_retention_period
          - data_subprocessors
          - data_transfer
          - declared_company_name
          - dpo_contact_address
          - dpo_contact_mail
          - dpo_contact_phone
          - user_rights
          - website
    UpdateProvidedInformation:
      type: object
      allOf:
        - $ref: '#/components/schemas/ProvidedInformationProperties'
    ProvidedInformationProperties:
      type: object  
      properties:
        owner:
          type: string
        category:
          type: string
        consent_withdraw:
          type: string
        country:
          type: string
        data_controller:
          type: string
        data_controller_contact_address:
          type: string
        data_controller_contact_mail:
          type: string
        data_controller_contact_phone:
          type: string
        data_controller_identity:
          type: string
        data_for_automatic_decision:
          type: string
        data_location:
          type: string
        data_processed:
          type: string
        data_processing_legal_basis:
          type: string
        data_processing_purposes:
          type: array
          minItems: 1
          items:
            type: string
            anyOf:
              - enum:
                - "Personalized advertising"
                - "Contextual advertising"
                - "Measuring and/or analysis"
                - "Investigation and/or research"
                - "Content personalisation"
                - "Propensity models"
                - "Match and combine with other data sources"
                - "Develop and/or improve products or services"
                - "Direct contact"
        data_processor:
          type: string
        data_recipients:
          type: array
          items:
            type: string
        data_retention_period:
          type: string
        data_subprocessors:
          type: array
          items:
            type: string
        data_transfer:
          type: string
        declared_company_name:
          type: string
        dpo_contact_address:
          type: string
        dpo_contact_mail:
          type: string
        dpo_contact_phone:
          type: string
        user_rights:
          type: string
        website:
          type: string
      additionalProperties: false
      example:
        owner: owner
        category: category
        consent_withdraw: consent_withdraw
        country: country
        data_controller: data_controller
        data_controller_contact_address: data_controller_contact_address
        data_controller_contact_mail: data_controller_contact_mail
        data_controller_contact_phone: data_controller_contact_phone
        data_controller_identity: data_controller_identity
        data_for_automatic_decision: data_for_automatic_decision
        data_location: data_location
        data_processed: data_processed
        data_processing_legal_basis: data_processing_legal_basis
        data_processing_purposes: 
          - "Measuring and/or analysis"
          - "Personalized advertising"
        data_processor: data_processor
        data_recipients:
          - data_recipient
          - data_recipient
        data_retention_period: data_retention_period
        data_subprocessors:
          - data_subprocessor
          - data_subprocessor
        data_transfer: data_transfer
        declared_company_name: declared_company_name
        dpo_contact_address: dpo_contact_address
        dpo_contact_mail: dpo_contact_mail
        dpo_contact_phone: dpo_contact_phone
        user_rights: users_rights
        website: website
    WebData:
      type: object
      allOf:
        - $ref: '#/components/schemas/WebDataProperties'
        - required:
          - company_name
    UpdateWebData:
      type: object
      allOf:
        - $ref: '#/components/schemas/WebDataProperties'
    WebDataProperties:
      properties:
        company_name:
          type: string
        operates_under:
          type: string
        categories:
          type: array
          items:
            type: string
        rank_in_category:
          type: integer
          format: int64
        connected_third_parties:
          type: array
          items:
            type: string
        connected_websites:
          type: array
          items:
            type: string
        tracking_devices:
          type: array
          items:
            type: string
        privacy_policy:
          type: string
        presence_in_security_lists:
          type: array
          items:
            type: string
        website:
          type: boolean
        third_party:
          type: boolean
        reach:
          type: integer
          format: int64
      additionalProperties: false
      example:
        website: true
        presence_in_security_lists:
        - presence_in_security_lists
        - presence_in_security_lists
        reach: 1
        company_name: company_name
        privacy_policy: privacy_policy
        connected_third_parties:
        - connected_third_parties
        - connected_third_parties
        tracking_devices:
        - tracking_devices
        - tracking_devices
        categories:
        - categories
        - categories
        third_party: true
        operates_under: operates_under
        rank_in_category: 6
        connected_websites:
        - connected_websites
        - connected_websites
    Scores:
      type: object
      allOf:
        - $ref: '#/components/schemas/ScoresProperties'
        - required:
          - privacy_score
          - security_score
          - transparency_score
    UpdateScores:
      type: object
      allOf:
        - $ref: '#/components/schemas/ScoresProperties'
    ScoresProperties:
      properties:
        privacy_score:
          type: number
          format: float
          minimum: -1.0
          maximum: 5.0
          multipleOf: 0.01
        security_score:
          type: number
          format: float
          minimum: -1.0
          maximum: 5.0
          multipleOf: 0.01
        transparency_score:
          type: number
          format: float
          minimum: -1.0
          maximum: 5.0
          multipleOf: 0.01
        privacy_motivation:
          type: array
          items:
            type: string
        security_motivation:
          type: array
          items:
            type: string
        transparency_motivation:
          type: array
          items:
            type: string
      additionalProperties: false
      example:
        privacy_score: 5.0
        transparency_score: 2.0
        security_score: 5.0
        transparency_motivation:
          - "Data location or transfer is outside EU."
    PrivacyMetric:
      required:
      - identifier
      - name
      - provided_information
      - scores
      - webdata
      type: object
      properties:
        identifier:
          type: string
        name:
          type: string
        provided_information:
          $ref: '#/components/schemas/ProvidedInformation'
        webdata:
          $ref: '#/components/schemas/WebData'
        scores:
          $ref: '#/components/schemas/Scores'
      additionalProperties: false
      example:
        scores:
          privacy_score: 5.0
          transparency_score: 2.0
          security_score: 5.0
          transparency_motivation:
            - "Data location or transfer is outside EU."
        provided_information:
          # Username of data buyer in charge of managing informartion contained in this PM
          owner: owner
          # Self-declared categorisation information based on business or mission
          category: category
          # Describes how the user can withdraw consent to data processing
          consent_withdraw: consent_withdraw
          # Establishment country as declared by the company
          country: country
          # Describes who owns the data subject to processing
          data_controller: data_controller
          # Physical address to contact the controller
          data_controller_contact_address: data_controller_contact_address
          # Mail address to contact the controller
          data_controller_contact_mail: data_controller_contact_mail
          # Phone to contact the controller
          data_controller_contact_phone: data_controller_contact_phone
          # Provides the identity of the controller, or controller's representative
          data_controller_identity: data_controller_identity
          # Describes whether the data will be used for automatic decision-making, including profiling and possible consequences for the user
          data_for_automatic_decision: data_for_automatic_decision
          # Descibres where data is going to be physically stored (in or outside EU)
          data_location: data_location
          # Describes which kind of data is collected and processed
          data_processed: data_processed
          # Describes whether the processing is based on Consent or Legitimate Interest
          data_processing_legal_basis: data_processing_legal_basis
          # Describes how processed data will be used by the company
          data_processing_purposes: 
            - data_processing_purposes
          # Describes who is going to process the data
          data_processor: data_processor
          # Describes who will receive the pesonal data (if any)
          data_recipients:
            - data_recipient
            - data_recipient
          # Describes how long data is going to be stored by the data owner, or the criteria used to determine the period
          data_retention_period: data_retention_period
          # Describes possible sub-processors involved in the data processing
          data_subprocessors:
            - data_subprocessor
            - data_subprocessor
          # Describes whether the controller plans to transfer personal data to a third country or international organization
          data_transfer: data_transfer
          # Company name as declared by the company
          declared_company_name: declared_company_name
          # Physical address to contact the DPO
          dpo_contact_address: dpo_contact_address
          # Mail address to contact the DPO
          dpo_contact_mail: dpo_contact_mail
          # Phone to contact the DPO
          dpo_contact_phone: dpo_contact_phone
          # Describes the resources where the user can get information to apply rights on her data
          user_rights: users_rights
          # Company's official website
          website: website
        name: name
        identifier: identifier
        webdata:
          website: true
          presence_in_security_lists:
          - presence_in_security_lists
          - presence_in_security_lists
          reach: 1
          company_name: company_name
          privacy_policy: privacy_policy
          connected_third_parties:
          - connected_third_parties
          - connected_third_parties
          tracking_devices:
          - tracking_devices
          - tracking_devices
          categories:
          - categories
          - categories
          third_party: true
          operates_under: operates_under
          rank_in_category: 6
          connected_websites:
          - connected_websites
          - connected_websites
    PrivacyMetrics:
      type: array
      items:
        $ref: '#/components/schemas/PrivacyMetric'
    Error:
      required:
      - code
      - message
      type: object
      properties:
        code:
          type: integer
          format: int32
        message:
          type: string
  securitySchemes:
    OAuth2:
      type: oauth2
      flows:
        clientCredentials:
          tokenUrl: !ENV ${TOKEN_URL}
          scopes:
            read:privacy-metrics: Grant read access to Privacy Metrics
            create:privacy-metric: Grant write access to single Privacy Metric
            delete:privacy-metric: Grant delete permission to single Privacy Metric
            update:webdata: Grants write access to Privacy Metric/Webdata
            update:scores: Grants write access to Privacy Metric/Scores
            update:provided-information: Grants write access to Privacy Metric/Provided
              Information
      x-tokenInfoFunc: privacy_metrics.controllers.authorization_controller.check_OAuth2
      x-scopeValidateFunc: privacy_metrics.controllers.authorization_controller.validate_scope_OAuth2

