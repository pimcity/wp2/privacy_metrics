# coding: utf-8

from __future__ import absolute_import

from privacy_metrics.test import BaseTestCase


class TestDefaultController(BaseTestCase):
    """DefaultController integration test stubs"""

    def test_health_get(self):
        """Test case for health_get

        Checks if the server is running
        """
        response = self.client.open("/privacy-metrics//health", method="GET")
        self.assert200(response, "Response body is : " + response.data.decode("utf-8"))


if __name__ == "__main__":
    import unittest

    unittest.main()
