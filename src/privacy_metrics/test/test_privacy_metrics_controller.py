# coding: utf-8

from __future__ import absolute_import

from flask import json

from privacy_metrics.models.privacy_metric import PrivacyMetric  # noqa: E501
from privacy_metrics.models.provided_information import (  # noqa: E501
    ProvidedInformation,
)
from privacy_metrics.models.scores import Scores  # noqa: E501
from privacy_metrics.models.web_data import WebData  # noqa: E501
from privacy_metrics.test import BaseTestCase


class TestPrivacyMetricsController(BaseTestCase):
    """PrivacyMetricsController integration test stubs"""

    def test_create_privacy_metric(self):
        """Test case for create_privacy_metric

        Create a new Privacy Metric
        """
        body = [PrivacyMetric()]
        response = self.client.open(
            "/privacy-metrics//privacy-metrics",
            method="POST",
            data=json.dumps(body),
            content_type="application/json",
        )
        self.assert200(response, "Response body is : " + response.data.decode("utf-8"))

    def test_delete_privacy_metric(self):
        """Test case for delete_privacy_metric

        Deletes Privacy Metric
        """
        response = self.client.open(
            "/privacy-metrics//privacy-metrics/{id}".format(id=789), method="DELETE"
        )
        self.assert200(response, "Response body is : " + response.data.decode("utf-8"))

    def test_get_privacy_metric(self):
        """Test case for get_privacy_metric

        Get specific Privacy Metric
        """
        response = self.client.open(
            "/privacy-metrics//privacy-metrics/{id}".format(id=789), method="GET"
        )
        self.assert200(response, "Response body is : " + response.data.decode("utf-8"))

    def test_list_privacy_metrics(self):
        """Test case for list_privacy_metrics

        Get services for which we have a Privacy Metric
        """
        query_string = [("limit", 50), ("offset", 1)]
        response = self.client.open(
            "/privacy-metrics//privacy-metrics", method="GET", query_string=query_string
        )
        self.assert200(response, "Response body is : " + response.data.decode("utf-8"))

    def test_modify_provided_information(self):
        """Test case for modify_provided_information

        Modifies specific Privacy Metric/Provided Information
        """
        body = ProvidedInformation()
        response = self.client.open(
            "/privacy-metrics//privacy-metrics/{id}/provided-information".format(
                id=789
            ),
            method="PATCH",
            data=json.dumps(body),
            content_type="application/json",
        )
        self.assert200(response, "Response body is : " + response.data.decode("utf-8"))

    def test_modify_scores(self):
        """Test case for modify_scores

        Modifies specific Privacy Metric/Scores
        """
        body = Scores()
        response = self.client.open(
            "/privacy-metrics//privacy-metrics/{id}/scores".format(id=789),
            method="PATCH",
            data=json.dumps(body),
            content_type="application/json",
        )
        self.assert200(response, "Response body is : " + response.data.decode("utf-8"))

    def test_modify_webdata(self):
        """Test case for modify_webdata

        Modifies specific Privacy Metric/Webdata
        """
        body = WebData()
        response = self.client.open(
            "/privacy-metrics//privacy-metrics/{id}/webdata".format(id=789),
            method="PATCH",
            data=json.dumps(body),
            content_type="application/json",
        )
        self.assert200(response, "Response body is : " + response.data.decode("utf-8"))

    def test_update_provided_information(self):
        """Test case for update_provided_information

        Substitutes specific Privacy Metric/Provided Information
        """
        body = ProvidedInformation()
        response = self.client.open(
            "/privacy-metrics//privacy-metrics/{id}/provided-information".format(
                id=789
            ),
            method="PUT",
            data=json.dumps(body),
            content_type="application/json",
        )
        self.assert200(response, "Response body is : " + response.data.decode("utf-8"))

    def test_update_scores(self):
        """Test case for update_scores

        Substitutes specific Privacy Metric/Scores
        """
        body = Scores()
        response = self.client.open(
            "/privacy-metrics//privacy-metrics/{id}/scores".format(id=789),
            method="PUT",
            data=json.dumps(body),
            content_type="application/json",
        )
        self.assert200(response, "Response body is : " + response.data.decode("utf-8"))

    def test_update_webdata(self):
        """Test case for update_webdata

        Substitutes specific Privacy Metric/Webdata
        """
        body = WebData()
        response = self.client.open(
            "/privacy-metrics//privacy-metrics/{id}/webdata".format(id=789),
            method="PUT",
            data=json.dumps(body),
            content_type="application/json",
        )
        self.assert200(response, "Response body is : " + response.data.decode("utf-8"))


if __name__ == "__main__":
    import unittest

    unittest.main()
