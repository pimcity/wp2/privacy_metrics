#!/bin/bash

docker-compose down && docker rmi `docker images | grep "privacy_metrics" | awk '{print $3}'`