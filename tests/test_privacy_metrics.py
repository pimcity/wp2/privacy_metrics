from privacy_metrics import __version__


def test_version() -> None:
    assert isinstance(__version__, str)
    assert __version__ != ""
